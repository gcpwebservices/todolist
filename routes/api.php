<?php

use Illuminate\Http\Request;
use App\User;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/users', function () {
  	$ulist = User::get();

  	return json_encode($ulist);

});


// Route::resource('/vueform', 'HomeController', [
//   'except' => ['edit', 'show']
// ]);

Route::resource('vueform', 'Todos');
