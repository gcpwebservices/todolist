<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $fillable = [
        'owner','name','details'
    ];
    protected $table = 'todos';
    public $timestamps = true;
}
