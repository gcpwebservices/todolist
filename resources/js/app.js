require('./bootstrap');
window.Vue = require('vue');


import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest'
};


import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue);

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

let MainComponent = Vue.component('maincomponent', require('./components/MainComponent.vue'));

const app = new Vue({
    el: '#app',
    components: {
		MainComponent
    }
});
